Okay there's a few different kinds of emails

1. Flavor mail
   - This stuff has no game mechanic implications
   - It's just stuff to distract you
   - Inline images are good but not required

2. Story-based email
   - This is a bit harder to do
   - Email is linked together in weird ways
   - Some of these emails add new emails to the pool

3. Gameplay loop mail
   - This is randomly generated
   - With a sample of introductory text
