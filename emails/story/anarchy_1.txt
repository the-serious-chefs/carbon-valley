tyler@juneau.com
Activating Account

Howdy! I'm trying to get set up with a new account at OGREL. Here's my
account information:

Name: Tyler Auclair
DOB: 02/25/1976
Department: Accounting


For some reason I can't get in through the OGREL firewall so I had to use my
personal email. Apologies!

-- 

Tyler

[A] Activate Tyler's account
-10 -5 5
You activate the account.
[MEM] An unauthorized user managed to get an account at OGREL. They tried to badge in but our security staff promptly apprehended the criminal.
[B] Don't activate the account
0 0 -5
This email is pretty fishy so you just ignore it.
[ADD] anarchy_2.txt