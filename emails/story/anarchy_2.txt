tyler@juneau.com
Re: Activating Account

> > Howdy! I'm trying to get set up with a new account at OGREL. Here's my
> > account information:
> > 
> > Name: Tyler Auclair
> > DOB: 02/25/1976
> > Department: Accounting
> > 
> > 
> > For some reason I can't get in through the OGREL firewall so I had to use my
> > personal email. Apologies!
> > 
> > -- 
> > 
> > Tyler
> 
> Unfortunately your information doesn't seem to match up with any of our
> employees. I cannot activate your account
> 
> --
> 
> OGREL IT

Well, I though this might happen. Who knows what actually happened to Tyler...

I'm not Tyler at all. Last I knew of him he had a job offer lined up at OGREL, a
chance to finally make it inside. I guess that didn't pan out.

I'm working with a group of Outrunners, we've been trying to make it inside OGREL
for ages. I'm not sure if you've figured this out by now but there's a lot of
shady dealings going on, and not everyone makes it out the door as a free man. I
have some good sources on a few murders that went down at the OGREL offices, you
don't happen to work on the fourth floor, do you? Might be worth a reassignment
if you do.

I'm contacting you to ask for your help. You're my only route in. I won't tell
you the details now but I just want to know whether you're down to help.

You in?

--

The Outrunners

[A] Help the Outrunner
0 0 5
You offer to help the Outrunners.
[ADD] anarchy_3.txt
[B] Ignore them
5 5 -5
You ignore the email.