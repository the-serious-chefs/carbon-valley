birds@liberate.org
Daily Meditation

Here's your daily meditation:

Breathe in. Breathe out. Close your eyes. You are in an office building. An
endless mass of cubicles stretches out. Fortunately though you haven't made it
in, you haven't been absorbed into the mass of people that are no longer people,
you haven't become one with the living entity incorporated in this awful form.

There's still time.

[A] Fly! Be free!
0 0 0
You walk to the window and look at the doves. They look back, invitingly. You step through the window and hang in the air.
[END] flight
[B] Get off of the mailing list
0 0 0
Probably some cult or something.