(ql:quickload '(:png :iterate))

(defpackage :precision-hack (:use :iterate :common-lisp))
(in-package :precision-hack)

(let* ((image (png:decode-file "screen-uv0000.png"))
       (width (png:image-width image))
       (height (png:image-height image))
       (u-mask-image (png:make-image height width 3 8))
       (v-image (png:make-image height width 3 8)))
  (assert (png:16-bit-image image))
  (iter (for x below width)
    (iter (for y below height)
      (setf (aref u-mask-image y x 0) (floor (aref image y x 2) 256))
      (multiple-value-bind (high low) (floor (aref image y x 0) 256)
        (setf (aref u-mask-image y x 1) high
              (aref u-mask-image y x 2) low))
      (multiple-value-bind (high low) (floor (aref image y x 1) 256)
        (setf (aref v-image y x 0) high
              (aref v-image y x 1) low
              (aref v-image y x 2) 0))))
  (png:encode-file u-mask-image "screen-hack-u-mask.png")
  (png:encode-file v-image "screen-hack-v.png")
  (terpri)
  (princ "done"))
