extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
    get_node("/root").remove_child(get_node("/root/text_box"))
    pass # Replace with function body.

func _input(event):
    if event is InputEventMouseButton && event.is_pressed():
        get_tree().change_scene("res://scenes/Credits.tscn")
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
