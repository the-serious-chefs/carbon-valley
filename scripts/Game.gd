extends Control

var helper

var helperpath = "res://scripts/MailHelper.gd"
var maildatapath = "res://scripts/MailData.gd"
var mailobjpath = "res://scripts/MailObject.gd"
var emailitempath = "res://scenes/EmailItem.tscn"
var spath = "res://emails/story"
var jpath = "res://emails/junk"
var tpath = "res://emails/support"

var clockLabel
var Vboxcontainer
var EmailContainer
var EmailSiteContainer
var EmailWindowScrollContainer
var BodyLabel
var OptionALabel
var OptionBLabel

var game_time = 0
var paused = false
var clockMinutes = 0
var clockHours = 8
var clockFormat = "am"
var START_TIME = 8
var END_TIME = 5
var day = 0

# PLAYER RESOURCES
var corporateApproval = 50
var coworkerApproval = 50
var morality = 50

const INBOX_SIZE = 10

var junkMail = []
var storyMail = []
var jobMail = []

var storyPool = ["first_day.txt","introduction.txt","nigeria_1.txt","fly_1.txt"]

# an array of MailObjects
var currentInboxObects = []
var nextDayEmails = []
var currentMailIndex = 0

var memoAdditions = []

var reviewObj
var review_active = false

# Called when the node enters the scene tree for the first time.
func _ready():
    randomize()
    clockLabel = get_node("TaskbarRect/ClockRect/ClockLabel")
    Vboxcontainer = get_node("BackgroundPanel/EmailSiteWindowRect/EmailListRect/ScrollContainer/VBoxContainer")
    EmailContainer = get_node("BackgroundPanel/EmailWindowRect")
    EmailSiteContainer = get_node("BackgroundPanel/EmailSiteWindowRect")
    EmailWindowScrollContainer = get_node("BackgroundPanel/EmailWindowRect/ScrollContainer2")
    BodyLabel = get_node("BackgroundPanel/EmailWindowRect/ScrollContainer2/VBoxContainer/BodyLabel")
    OptionALabel = get_node("BackgroundPanel/EmailWindowRect/ScrollContainer2/VBoxContainer/OptionALabel")
    OptionBLabel = get_node("BackgroundPanel/EmailWindowRect/ScrollContainer2/VBoxContainer/OptionBLabel")
    reviewObj = load(mailobjpath).new()
    helper = load(helperpath).new()
    helper.gen_rand_junk_mail_stuff()
    set_process(true)
    load_mails()
    populate_in_box()
    pass # Replace with function body.

func _process(delta):
    if not paused:
        game_time += delta
        set_time(game_time*10)

func load_mails():
    print("====================================================")
    print("     IMPORTING EMAILS")
    print("====================================================")
    var targ_path = spath
    var storymails = list_files_in_directory(targ_path)
    for item in storymails:
        var loc_path = targ_path + "/" + item
        var object = load(maildatapath).new(self,loc_path)
        storyMail.append(object)
    targ_path = tpath
    var techmails = list_files_in_directory(targ_path)
    for item in techmails:
        var loc_path = targ_path + "/" + item
        var object = load(maildatapath).new(self,loc_path)
        jobMail.append(object)
    targ_path = jpath
    var junkmails = list_files_in_directory(targ_path)
    for item in junkmails:
        var loc_path = targ_path + "/" + item
        var object = load(maildatapath).new(self,loc_path)
        junkMail.append(object)
    print("\n")

func list_files_in_directory(path):
    var files = []
    var dir = Directory.new()
    dir.open(path)
    dir.list_dir_begin()
    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with("."):
            files.append(file)
    dir.list_dir_end()
    return files

func save_text_file(text, path):
    var f = File.new()
    var err = f.open(path, File.WRITE)
    if err != OK:
        printerr("Could not write file, error code ", err)
        return
    f.store_string(text)
    f.close()

func load_text_file(path):
    var f = File.new()
    var err = f.open(path, File.READ)
    if err != OK:
        printerr("Could not open file [" + path + "], error code ", err)
        return ""
    var text = f.get_as_text()
    f.close()
    return text

func set_time(time):
    var minutes = round(time/60)
    if(minutes >= 60):
        game_time = 0
        clockHours += 1
    if (clockHours >= 12):
        clockFormat = "pm"
    if (clockHours >= 13):
            clockHours -= 12
    var clockTime = round(time)
    var hourString = String(clockHours)
    var minuteString = String(minutes).pad_zeros(2)
    var timestring = hourString + ":" + minuteString + clockFormat
    clockLabel.text = timestring
    return

func check_time():
    clockHours += 2
    if(clockHours >= END_TIME):
        if(clockHours < START_TIME):
            return true
    return false

func rand_thing(list):
    if (list != null):
        if (list.size() != 0):
            var i = randi()%list.size()
            return list[i]
    return null

func get_story_email(name):
    for mail in storyMail:
        if mail.datapath == ("res://emails/story/%s" % name):
            return mail
    return null

func populate_in_box():
    # remove all current children
    for i in range(0, Vboxcontainer.get_child_count()):
        Vboxcontainer.remove_child(Vboxcontainer.get_child(0))
    currentInboxObects = []
    
    print("====================================================")
    print("     POPULATING INBOX")
    print("====================================================")

    var counter = 0;
    # Add everything from the mail pool
    for mail in storyPool:
        var object = get_story_email(mail).generateObject()
        currentInboxObects.append(object)
        counter += 1
    # Reset the mail pool
    storyPool = []


    for i in range(counter,INBOX_SIZE):
        if randi() % 3 == 1:
            # add junk mail
            if (junkMail.size() > 0):
                var obj = rand_thing(junkMail).generateObject()
                currentInboxObects.append(obj)
        else:
            # add job mail
            if (jobMail.size() > 0):
                var obj = rand_thing(jobMail).generateObject()
                currentInboxObects.append(obj)
    


    print("Inbox size = " + String(currentInboxObects.size()))

    # randomize the order of the mail
    currentInboxObects.shuffle()

    # actually create the objects
    for i in range(currentInboxObects.size()):
        var obj = currentInboxObects[i]

        var email_instance = load(emailitempath).instance()
        email_instance.set_name("email"+String(i))  # prevents unexpected names
        var author_line = email_instance.get_node("AuthorLabel")
        var subject_line = email_instance.get_node("AuthorLabel2")
        author_line.text = obj.AuthorString
        subject_line.text = obj.SubjectString

        # bring our email into the world
        Vboxcontainer.add_child(email_instance)

    var buttons = get_tree().get_nodes_in_group("email_button")
    for i in range(0,buttons.size()):
        var button = buttons[i]
        button.connect("pressed", self, "_which_button_pressed", [i,button])


func return_to_mail(pickedA):
    var resultsMod
    var obj
    var afterText
    var commands
    var doNotShowMessage = false
    var review_was_active = review_active
    if(review_active):
        obj = reviewObj
        review_active = false
        doNotShowMessage = true
    else:
        obj = currentInboxObects[currentMailIndex]
    if(pickedA):
        resultsMod = obj.OptionAResults
        commands = obj.OptionACommands
        afterText = obj.OptionAfterStrings[0]
    else:
        resultsMod = obj.OptionBResults
        commands = obj.OptionBCommands
        afterText = obj.OptionAfterStrings[1]
    corporateApproval += resultsMod[0]
    coworkerApproval += resultsMod[1]
    morality += resultsMod[2]
    
    var emailnode = get_node("BackgroundPanel/EmailSiteWindowRect/EmailListRect/ScrollContainer/VBoxContainer/email" + String(currentMailIndex))
    if not review_was_active:
        Vboxcontainer.remove_child(emailnode)
    #currentInboxObects.remove(currentMailIndex)
    if(!doNotShowMessage):
        var myDumbArray = []
        var myPool = afterText.split('\n')
        for i in range(0,myPool.size()-1):
            myDumbArray.append(myPool[i])
        print(myDumbArray)
        get_node("/root/text_box").show_lines(myDumbArray)
    
    for command in commands:
        var commandType = command[0]
        var instruction = command[1]
        if(commandType == "[ADD]"):
            # add an email to next day
            storyPool.append(instruction)
            continue
        elif(commandType == "[MEM]"):
            # add something to today's memo
            memoAdditions.append(instruction)
            continue
        elif(commandType == "[END]"):
            # execute particular game ending
            get_node("/root/jump").trigger_ending(instruction)
            continue

    EmailContainer.visible = false
    EmailSiteContainer.visible = true

func _which_button_pressed(index, button):
    var obj = currentInboxObects[index]
    BodyLabel.text = obj.BodyString
    OptionALabel.text = obj.OptionStrings[0]
    OptionBLabel.text = obj.OptionStrings[1]
    currentMailIndex = index
    EmailWindowScrollContainer.scroll_vertical = 0
    EmailContainer.visible = true
    EmailSiteContainer.visible = false
    

func calculate_review(pickedA):
    var reviewBody
    var youLose = false
    var corporateText
    var afterText
    if(pickedA):
        afterText = currentInboxObects[currentMailIndex].OptionAfterStrings[0]
    else:
        afterText = currentInboxObects[currentMailIndex].OptionAfterStrings[1]
    var myDumbArray = []
    var myPool = afterText.split('\n')
    for i in range(0,myPool.size()-1):
        myDumbArray.append(myPool[i])
    print(myDumbArray)
    get_node("/root/text_box").show_lines(myDumbArray)
    
    if(corporateApproval >= 90):
        corporateText = "It seems the top brass couldn't be happier with you! Good job, I guess."
    elif(corporateApproval >= 80):
        corporateText = "Your name frequently comes up in meetings. In a good way, no less."
    elif(corporateApproval >= 70):
        corporateText = "Upstairs is considering giving you a raise."
    elif(corporateApproval >= 60):
        corporateText = "I've heard some good things about you."
    elif(corporateApproval >= 50):
        corporateText = "One of the execs has a son named after you. A good omen."
    elif(corporateApproval >= 40):
        corporateText = "It seems nobody upstairs has heard your name before. Try to keep it that way."
    elif(corporateApproval >= 30):
        corporateText = "Upper management is somewhat displeased with you."
    elif(corporateApproval >= 20):
        corporateText = "Upper managemnt is using your name as an adjective. The nature of the adjective makes it impolite to greet you."
    elif(corporateApproval >= 10):
        corporateText = "You've been making an impression with upper management. A BAD ONE!"
    elif(corporateApproval >= 0):
        corporateText = "Why are you still here? The execs hate you!"
    else:
         # you lose
        corporateText = "We both saw this coming. You aren't cut out for this. Go work somewhere where the management can tolerate your inexistance."
        youLose = true
    
    var coworkerText
    if(coworkerApproval >= 90):
        coworkerText = "A smile comes over someone's face when your name is mentioned. Keep it up."
    elif(coworkerApproval >= 80):
        coworkerText = "Your coworkers seem to like you a lot."
    elif(coworkerApproval >= 70):
        coworkerText = "It seems you have begun to fit in pretty well on your floor."
    elif(coworkerApproval >= 60):
        coworkerText = "One of your coworkers said something nice about you"
    elif(coworkerApproval >= 50):
        coworkerText = "I heard you say 'good morning' to a friend of yours. I'm glad you have A friend."
    elif(coworkerApproval >= 40):
        coworkerText = "It seems the people on your floor don't have an opinion of you."
    elif(coworkerApproval >= 30):
        coworkerText = "Your coworkers have a bad impression of you."
    elif(coworkerApproval >= 20):
        coworkerText = "People avoid you in the break room."
    elif(coworkerApproval >= 10):
        coworkerText = "I heard it on good authority that no one will even give you the time of day."
    elif(coworkerApproval >= 0):
        coworkerText = "Are you enjoying the lonely life of everyone hating you?"
    else:
         # you lose
        coworkerText = "It seems nobody wants to show up to work because of you. It's time you don't show your face around here anymore."
        youLose = true
    
    var moralityText
    if(morality >= 90):
        moralityText = "You are the definition of a good person."
    elif(morality >= 80):
        moralityText = "Your morals are pretty good!"
    elif(morality >= 70):
        moralityText = "I'm impressed with your decent morals."
    elif(morality >= 60):
        moralityText = "I heard you did some good deeds recently."
    elif(morality >= 50):
        moralityText = "Every now and then you seem to do something moderately moral."
    elif(morality >= 40):
        moralityText = "You should consider yourself neither moral nor immoral."
    elif(morality >= 30):
        moralityText = "You're a relatively average, not good, person."
    elif(morality >= 20):
        moralityText = "The moral highground is not something you are an authority on."
    elif(morality >= 10):
        moralityText = "Considering everything you've done, you are not a good person."
    elif(morality >= 0):
        moralityText = "You are a terrible person. How can you live with yourself!"
    else:
         # you lose
        moralityText = "In terms of support ratings, we have to let you go. Go work somewhere where being an awful person is acceptable."
        youLose = true
    
    reviewBody = "\n" + corporateText + "\n\n" + coworkerText + "\n\n" + moralityText + "\n\n"
    for thing in memoAdditions:
        reviewBody += thing + "\n"
        
    memoAdditions = []
    if(!youLose):
        reviewBody += "--\n"
        reviewBody += "Work hard and you can stay in work\n"
    reviewBody += "\nDan Farming"
    
    reviewObj.AuthorString = "authdan@ogrel.dev"
    reviewObj.SubjectString = "Performace Review"
    reviewObj.BodyString = "dan@ogrel.dev\nPerformace Review\n\n" + reviewBody
    reviewObj.OptionStrings = ["<Complain to coworkers about the lowlife that is Dan Farming>", "<Put the performance review behind you>"]
    reviewObj.OptionAfterStrings = ["Who does he thing he is, spending his every waking momen micromanaging you!",
                                        "\"Maybe one day you'll take his place\", or so you tell yourself."]
    reviewObj.OptionAResults = [-4,3,-1]
    reviewObj.OptionBResults = [0,0,0]
    reviewObj.OptionACommands = []
    reviewObj.OptionBCommands = []
    
    if (youLose):
        reviewObj.OptionACommands = ["LOSE"]
        reviewObj.OptionACommands = ["LOSE"]
func display_end_of_day_review(pickedA):
    calculate_review(pickedA)
    var obj = reviewObj
    BodyLabel.text = obj.BodyString
    OptionALabel.text = obj.OptionStrings[0]
    OptionBLabel.text = obj.OptionStrings[1]
    EmailWindowScrollContainer.scroll_vertical = 0
    EmailContainer.visible = true
    EmailSiteContainer.visible = false
    
    populate_in_box()

func reset_time():
    clockHours = START_TIME - 2
    clockFormat = "am"
    set_time(0)

func _on_ButtonA_pressed():
    if(review_active):
        reset_time()
    if(check_time()):
        if _maybe_end():
            return
        #enter performance review
        review_active = true
        display_end_of_day_review(true)
        return
    return_to_mail(true)

func _on_ButtonB_pressed():
    if(review_active):
        reset_time()
    if(check_time()):
        if _maybe_end():
            return
        #enter performance review
        review_active = true
        display_end_of_day_review(false)
        return
    return_to_mail(false)
    

func _maybe_end():
        day = day + 1
        if day > 7:
            if corporateApproval > 50:
                get_node('/root/jump').trigger_ending("retirement")
            else:
                get_node('/root/jump').trigger_ending("fired")
            return true
        else:
            return false
    
