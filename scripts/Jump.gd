extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
    # anarchy()
    pass

func anarchy():
    var branch = [ "Are you thinking what I'm thinking?",
["Plant the charges",
["You grab the charges. There's not much time to lose, you can hear whoever's following you sprinting down the stairs. You go out into the lobby.",
"The receptionist sees you and screams. She cowers behind the front desk as you make your way to the one entrance to the basement.",
"You swipe in.",
"The building alarms start going off. Not much time now. You sprint down the stairs and think of all the awful things OGREL has done to you and everyone you know.", 
"You wonder how there could be so many workplace deaths in a place as mundane as an office tower.",
"You pull out a charge. Each one of the C-4 charges in the bag you picked up have a number on them.",
"Just like the server racks in the basement!",
"You hurry around the datacenter, planting each of the charges in the right place. There's a few missing but it seems to cover the basement pretty well.",
"The door to the basement explodes. You dive to the ground.",
"\"This is the police. You are a wanted terrorist. You can end this the easy way or the hard way.\"",
"Fuck. Guess there's no negotiating.",
"The police descend down the stairs and you realize there's not a lot of time left.",
[
"But, you realize you have one last choice to make.",
[
"Throw away the detonator", 
["You throw the detonator to the ground. The police don't take long to find you among the server racks.",
"The receptionist doesn't even hear the gunshots.",
null]],

["Detonate the C-4",
["So. This is what it's come to.", 
"You don't really have any options left. It was probably a mistake to even try helping the Outrunners, but maybe you'll achieve in death what you never could in life.",
"- KABOOM -",
null]]]]],

["Run",
["You open the door to outside and start running. You hear the door open behind you but you don't stop.",
"You make it to the parking garage. You get in the car and put the key in the ignition.",
"Reeeeeee",
"Reeeeeeeeeeeeeee",
"Reeeeeeeee -- THRROOOM",
"Pulling out of your daily spot at max speed you clip a pole. You didn't really need that mirror anyways. You drive out.",
"There's sirens, but they quickly fade in the distance. All you know is that you're never coming back.",
null]]]
    var anarchy = [
"Your ears perk up at the sound of your name.",
"The sound is kind of distant but your paranoia kicks in just the same. The timing with this email is just too eerie, it must be one of the Outrunners.",
"You grab your briefcase and duck out through the entrance to your cubicle. The sound of fans and cheap elevator music is punctuated by harsh footsteps.",
"A man dressed entirely in black steps into view at the end of the cube aisle.",
"You duck down another alleyway. This place is a maze, but it's a maze you've had to navigate too many times. Interning here gave you plenty of experience navigation this labyrinth at high speeds. Too slow and the coffee gets cold.",
"The footsteps speed up. Whoever's here is practically running now and you start to do the same.",
"You make it to the stairs.",
"You fly down them as fast as you can, taking them two, three, or four at a time.",
"You reach the bottom.",
"But something's off, you see something weird under the stairwell on the first floor. Wait a minute, are those explosives?",
"It's the explosives from the Outrunner that tried to blow this place sky high. It's a fairly large amount of C-4 charges and you wonder how everyone missed it. They must be taking the elevator or something.",
"Next to the pile you see a keycard. The name reads \"Stephen Stile\".", 
branch]
    get_node("/root/text_box").show_lines(anarchy)

func nigeria():
    get_node("/root/text_box").show_lines(["You live out the rest of your days as the king of Nigeria.",null])

func killed():
    var kill = [
"It's the end of your work day, much like any other.",
"You pack up your belongings and get ready to head home.",
"You step out of your cube.",
"And feel a knife pierce into your back.",
null
]
    get_node("/root/text_box").show_lines(kill)

func overdose():
    var death = [
"You're hanging out with some of your work friends.",
"It's a party at your place, complete with strippers and cocaine.",
"Well, you think it's cocaine at least. Whatever it was, it wasn't good for you.",
"You spasm and die.",
null
]
    get_node("/root/text_box").show_lines(death)


func flight():
    get_node("/root/text_box").show_lines(["[You walk to the window and look at the doves. They look back, invitingly. You step through the window and hang in the air.",null])

func retirement():
    var retirement = [
"You made it through your first week and passed the performance review! Looks like you managed to hold down your job, it should be smooth sailing from here.",
"You pull open your email and zone out, your job practically does itself now.",
"Day 2 of the rest of your life. You pull open your email and get to work.",
"Day 5 of the rest of your life. You pull open your email and get to work.",
"Day 30 of the rest of your life. You pull open your email and get to work.",
"Wow, you can't believe that you made it through an entire month at this place.",
"Day 145 of the rest of your life. You pull open your email and get to work.",
"Day 358 of the rest of your life. You pull open your email and get to work.",
"1 year anniversary! You have all your coworkers over for a party.",
"Day 1024 of the rest of your life. You pull open your email and get to work.",
"Day 2048 of the rest of your life. You pull open your email and get to work.",
"Day 4096 of the rest of your life. You pull open your email and get to work.",
"Day 8128 of the rest of your life. You pull open your email and get to work.",
"It's a perfect number! Sounds like a reason to party.",
"Day 18000 of the rest of your life. You pull open your email and get to work.",
"And go through your email one last time.",
"Because it's finally time to retire.",
"Wow, that sure went fast. Your whole life seemed to pass in minutes.",
"Anyways, time to enjoy what little life you have left.",
null
]
    get_node("/root/text_box").show_lines(retirement)

func fired():
    var fired = [
"You didn't cut it. Your first performance review came in and your results were lackluster at best.",
"Maybe you can find work at another software corp, but worst case you can probably flip patties at the local MegaBurger for the rest of your life",
null]
    get_node("/root/text_box").show_lines(fired)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
func trigger_ending(ending):
    # Remove the main game screen
    get_node("/root").remove_child(get_node("/root/CubicleScene"))
    match ending:
        "anarchy":
            anarchy()
        "nigeria":
            nigeria()
        "coworkers":
            killed()
        "morality":
            overdose()
        "flight":
            flight()
        "retirement":
            retirement()
        "fired":
            fired()
