extends Node

# THIS CLASS CONTAINS ALL THE DATA RELATED TO ONE EMAIL TEXT FILE

# TYPES:
# "TECH" = [rand-email-personal]@[rand-personal-mail-site]
# "JUNK" = 
# "<someone's name>" author not generated
var AuthorString
var dataString
var SubjectLines = []
var BodyString

var OptionStrings = ["OPTION A NULL","OPTION B NULL"]
var OptionAfterStrings = ["AFTER A NULL","AFTER B NULL"]
var OptionAResults = [0,0,0]
var OptionBResults = [0,0,0]
var OptionACommands = []
var OptionBCommands = []

var Game
var Helper
var datapath

func _init(game, path):
    Game = game
    Helper = game.helper
    datapath = path
    grabInfo()

func decodeAuthor(line):
    var auth = line
    if (auth == null):
        AuthorString = "NULL"
        print("null")
        return
    if (auth.begins_with("[rand-email-personal]")):
        auth = "TECH"
    AuthorString = auth

func decodeSubject(line):
    var subjects = line.strip_edges().split('|',true)
    print("decoding: " + line)
    for s in subjects:
        print("subject: " + String(s))
        SubjectLines.append(s)
        
func formatError(reason):
    print("\n")
    print("----------------- ERROR -----------------")
    print("FILE [" + datapath + "] is not formatted correctly. REFER TO THE TEMPLATE!")
    print("REASON: " + reason)
    print("----------------- ERROR -----------------")
    print("\n")

func decodeBody():
    var bodystartline = -1
    var bodyendline = -1
    var lines = dataString.split('\n',true)
    var bodytext = ""
    for i in range(2,lines.size()):
        lines[i] += "\n"
        if (bodystartline == -1):
            # we haven't found the start yet
            if(lines[i] == "\n"):
                continue
            else:
                bodytext += "\n" + lines[i]
                bodystartline = i
        else:
            if(!lines[i].begins_with("[A]")):
                bodytext += lines[i]
                continue
            else:
                bodyendline = i
                OptionStrings[0] = lines[i].substr(4).strip_escapes()
                var numberStringsA =lines[i+1].split(' ')
                if(numberStringsA.size() != 3):
                    formatError("Results A didn't work")
                    break
                for a in range(0,3):
                    OptionAResults[a] = numberStringsA[a].to_int()
                
                break
    var foundB = false
    var foundBString = false
    for i in range(bodyendline+2,lines.size()):
        var command = lines[i].substr(6).strip_escapes()
        var commandStringId = lines[i].left(5)
        command = [commandStringId,command]
        if(!lines[i].begins_with("[B]")):
            if(foundB == false):
                if(lines[i].begins_with("[ADD]")):
                    OptionACommands.append(command)
                    continue
                elif(lines[i].begins_with("[MEM]")):
                    OptionACommands.append(command)
                    continue
                elif(lines[i].begins_with("[END]")):
                    OptionACommands.append(command)
                    continue
                
                var linestring = lines[i].strip_escapes()
                if (OptionAfterStrings[0] == "AFTER A NULL"):
                    OptionAfterStrings[0] = linestring + "\n"
               # else:
                    #OptionAfterStrings[0] += linestring + "\n"
                    
            else:
                if(lines[i].begins_with("[ADD]")):
                    OptionBCommands.append(command)
                    continue
                elif(lines[i].begins_with("[MEM]")):
                    OptionBCommands.append(command)
                    continue
                elif(lines[i].begins_with("[END]")):
                    OptionBCommands.append(command)
                    continue
                
                #else:
                    #OptionAfterStrings[1] += linestring + "\n"
                
        else:
            foundB = true
            OptionStrings[1] = lines[i].substr(4).strip_escapes()
            #OptionAfterStrings[1] = lines[i+1].strip_escapes()
            var numberStringsB = lines[i+1].split(' ')
            if(numberStringsB.size() != 3):
                formatError("Results B didn't work")
                break
            for a in range(0,3):
                OptionBResults[a] = numberStringsB[a].to_int()
            var linestring = lines[i+2].strip_escapes()
            if (OptionAfterStrings[1] == "AFTER B NULL"):
                OptionAfterStrings[1] = linestring + "\n"
            #break
    lines = bodytext.split('\n',true)
    print(lines)
    var foundText = false
    var endBodyText = ""
    for i in range(0,lines.size()):
        var index = lines.size()-i-1
        if(lines[index] == ""):
            if(foundText):
                endBodyText = lines[index] + "\n" + endBodyText
                #endBodyText = endBodyText + lines[index] + "\n"
                continue
            else:
                foundText = true
        else:
            endBodyText = lines[index] + "\n" + endBodyText
            #endBodyText = endBodyText + lines[index] + "\n"
    BodyString = endBodyText
    print("JOSH " + datapath)
    print("JOSH " + String(OptionAfterStrings))

func grabInfo():
    # collect data from file and save useful bits of it
    dataString = Game.load_text_file(datapath)
    
    var rand_data = Helper.gen_rand_junk_mail_stuff()
    dataString = dataString.replace("<random name>", rand_data[0])
    dataString = dataString.replace("<name>", rand_data[0])
    var id = String(randi() % 999999 + 100000)
    dataString = dataString.replace("<random number>", id)
    dataString = dataString.replace("<ID>", id)

    var lines = dataString.split('\n',true)
    print("Loading file [" + datapath + "]")
    decodeAuthor(lines[0])
    decodeSubject(lines[1])
    decodeBody()

# actually puts together an email object from the data
func generateObject():
    print("========== generating " + String(datapath))
    var mailObject = load(Game.mailobjpath).new()
    var auth
    var helperstuff = Helper.gen_rand_junk_mail_stuff()
    var name = helperstuff[0]
    var randAuthString = helperstuff[1]
    if(AuthorString == "TECH"):
        auth = randAuthString
    else:
        auth = AuthorString
    print("Subject variations: " + String(SubjectLines))
    var sub = Game.rand_thing(SubjectLines)
    var BodyStringInstance = auth + "\n" + sub + "\n" + BodyString
    mailObject.AuthorString = auth
    mailObject.SubjectString = sub
    mailObject.BodyString = BodyStringInstance.replace("[rand-name]",name)

    mailObject.OptionStrings = OptionStrings
    mailObject.OptionAfterStrings = OptionAfterStrings
    mailObject.OptionAResults = OptionAResults
    mailObject.OptionBResults = OptionBResults
    mailObject.OptionACommands = OptionACommands
    mailObject.OptionBCommands = OptionBCommands
    mailObject.DataPath = datapath
    mailObject.debug()
    print("\n")
    return mailObject

func _ready():
    pass # Replace with function body.
