extends Node

# THIS CLASS IS FOR GENERATING EMAIL FIELDS

var NameList = ["Bill","Steve", "Rob", "Josh", "Jim","Thomas","Matthew","Eric",
"Mason","Jack","Tyson","Mike","Ruth","Jimmy","Joe","Alex","Jackson","Andrew",
"Wash","Henry","Cooper","Max","Derrick","Alice","Rose","Penny", "Stacy","Steph",
"Stephan","Madona","Jessica","Caroline","Valerie","Hunter","Robert","Charlie",
"Benjamin","James","Will","Harrison","Emma","Linda","Sarah","Herobrine",
"John",
"William",
"James",
"Charles",
"George",
"Frank",
"Joseph",
"Thomas",
"Henry",
"Robert",
"Edward",
"Harry",
"Walter",
"Arthur",
"Fred",
"Albert",
"Samuel",
"David",
"Louis",
"Joe",
"Charlie",
"Clarence",
"Richard",
"Andrew",
"Daniel",
"Ernest",
"Will",
"Jesse",
"Oscar",
"Lewis",
"Peter",
"Benjamin",
"Frederick",
"Willie",
"Alfred",
"Sam",
"Roy",
"Herbert",
"Jacob",
"Tom",
"Elmer",
"Carl",
"Lee",
"Howard",
"Martin",
"Michael",
"Bert",
"Herman",
"Jim",
"Francis",
"Harvey",
"Earl",
"Eugene",
"Ralph",
"Ed",
"Claude",
"Edwin",
"Ben",
"Charley",
"Paul",
"Edgar",
"Isaac",
"Otto",
"Luther",
"Lawrence",
"Ira",
"Patrick",
"Guy",
"Oliver",
"Theodore",
"Hugh",
"Clyde",
"Alexander",
"August",
"Floyd",
"Homer",
"Jack",
"Leonard",
"Horace",
"Marion",
"Philip",
"Allen",
"Archie",
"Stephen",
"Chester",
"Willis",
"Raymond",
"Rufus",
"Warren",
"Jessie",
"Milton",
"Alex",
"Leo",
"Julius",
"Ray",
"Sidney",
"Bernard",
"Dan",
"Jerry",
"Calvin",
"Perry",
"Dave",
"Anthony",
"Eddie",
"Amos",
"Dennis",
"Clifford",
"Leroy",
"Wesley",
"Alonzo",
"Garfield",
"Franklin",
"Emil",
"Leon",
"Nathan",
"Harold",
"Matthew",
"Levi",
"Moses",
"Everett",
"Lester",
"Winfield",
"Adam",
"Lloyd",
"Mack",
"Fredrick",
"Jay",
"Jess",
"Melvin",
"Noah",]

var PersonalMailSitesList = ["boohoo.com","coldmail.com","rol.net","box.net","gome.com","goggle.com","money.net"]

func _init():
    pass

func _ready():
    pass # Replace with function body.

func gen_name():
    var i = randi()%NameList.size()
    return NameList[i]
        
func gen_personal_mail_name(name):
    var chance = randi()%3
    var userName = name.to_lower()
    if (chance == 0):
        # throw random numbers on the end of the name
        userName += String(randi()%9999).pad_zeros(randi()%5)
    return userName

func gen_personal_mail_site():
    var i = randi()%PersonalMailSitesList.size()
    return PersonalMailSitesList[i]

func gen_rand_junk_mail_stuff():
    var name = gen_name()
    var siteString = gen_personal_mail_site()
    var userName = gen_personal_mail_name(name)
    var AuthorString = userName + "@" + siteString 
    #print(AuthorString)'
    return [name, AuthorString]
