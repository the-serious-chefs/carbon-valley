extends Node

# THIS CLASS IS USED AS A SINGLE INSTANCE OF AN EMAIL READY FOR DISPLAYING

var DataPath
var AuthorString
var SubjectString
var BodyString
var OptionStrings = []
var OptionAfterStrings = []
var OptionAResults = []
var OptionBResults = []
var OptionACommands = []
var OptionBCommands = []

func _init():
    pass

func debug():
    print("AuthorString = \"" + AuthorString + "\"")
    print("SubjectString = \"" + SubjectString + "\"") 

func _ready():
    pass # Replace with function body.
