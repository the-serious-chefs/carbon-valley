extends Node2D

export(Image) var screen_uv_texture

# The last processed input touch/mouse event. To calculate relative movement.
var last_mouse_pos_screen = null

onready var node_viewport = $Viewport
onready var node_compositor = $Compositor

func _input(event):
	# Check if the event is a non-mouse/non-touch event
	var is_mouse_event = false
	for mouse_event in [InputEventMouseButton, InputEventMouseMotion, InputEventScreenDrag, InputEventScreenTouch]:
		if event is mouse_event:
			is_mouse_event = true
			break

	if get_node("/root/text_box").active:
		return
	
	# If the event is a mouse/touch event and/or the mouse is either held or inside the area, then
	# we need to do some additional processing in the handle_mouse function before passing the event to the viewport.
	# If the event is not a mouse/touch event, then we can just pass the event directly to the viewport.
	if is_mouse_event:
		handle_mouse(event)
	elif not is_mouse_event:
		node_viewport.input(event)

# Handle mouse events inside Area. (Area.input_event had many issues with dragging)
func handle_mouse(event):
	# lookup in the screen uv texture to find the mouse position
	# mapped to the virtual crt screen
	var mouse_pos = get_global_mouse_position()
	# TODO: linear interpolation to make mouse coordinates smoother
	screen_uv_texture.lock()
	var mouse_pos_uv = screen_uv_texture.get_pixelv(mouse_pos)
	# flip vertically
	mouse_pos_uv.g = 1.0 - mouse_pos_uv.g
	screen_uv_texture.unlock()
	var mouse_pos_screen = node_viewport.size * Vector2(mouse_pos_uv.r, mouse_pos_uv.g)
	# Set the event's position and global position.
	event.position = mouse_pos_screen
	event.global_position = mouse_pos_screen
	
	# If the event is a mouse motion event...
	if event is InputEventMouseMotion:
		# If there is not a stored previous position, then we'll assume there is no relative motion.
		if last_mouse_pos_screen == null:
			event.relative = Vector2(0, 0)
		# If there is a stored previous position, then we'll calculate the relative position by subtracting
		# the previous position from the new position. This will give us the distance the event traveled from prev_pos
		else:
			event.relative = mouse_pos_screen - last_mouse_pos_screen
	last_mouse_pos_screen = mouse_pos_screen
	
	# Finally, send the processed input event to the viewport.
	node_viewport.input(event)
