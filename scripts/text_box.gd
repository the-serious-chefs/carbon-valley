extends Control

# This gets sent if the text is done displaying
signal text_over


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var active = false

# An array of chunks of text and sound effects
var text = []

# The text for the two optional branches
var text_a = []
var text_b = []


# Called when the node enters the scene tree for the first time.
func _ready():
    get_node("OptionContainer").visible = false
    visible = false
    mouse_filter=MOUSE_FILTER_STOP
    # show_lines(["Anime","time",["choice",["foo",["foo",null]],["bar",["bar",null]]]])

func _input(event):
    if self.active:
        if event is InputEventMouseButton && event.is_pressed() && event.button_index == 1:
            if not get_node("OptionContainer").visible:
                _advance_text()
                accept_event()

func _set_options(left,right):
    # Make the two buttons on the screen visible
    get_node("OptionContainer").visible = true
    # And set their text
    get_node("OptionContainer/HBoxContainer/MarginContainer/TextContainer/Text").text = left
    get_node("OptionContainer/HBoxContainer/MarginContainer2/TextContainer/Text").text = right

    mouse_filter=MOUSE_FILTER_PASS

func _add_a():
    show_lines(text_a)
    _continue_branch()

func _add_b():
    show_lines(text_b)
    _continue_branch()

func _continue_branch():
    get_node("OptionContainer").visible = false
    mouse_filter=MOUSE_FILTER_STOP

func _advance_text():
    # If we're out of text then stop displaying everything
    if text.empty():
        active = false
        emit_signal("text_over")
        return

    var next = text.pop_front()


    # Depending on what the next element is, display it or play it
    match typeof(next):
        TYPE_STRING:
            get_node("BoxContainer/TextContainer/Text").text = next

        # This one is a choice
        # The first element is the choice text
        # Second and third are more arrays
        # The first argument of those are the text, the second is a function
        TYPE_ARRAY:
            get_node("BoxContainer/TextContainer/Text").text = next[0]
            # Set the callbacks to be the right thing
            text_a = next[1][1]
            text_b = next[2][1]
            # And load up the buttons with the right text
            _set_options(next[1][0],next[2][0])

        # This one is a function to execute at the end of the text
        # But it's only in nightly :(
        # TYPE_CALLABLE:
        #     next.call()

        # End the game
        TYPE_NIL:
            get_tree().change_scene("res://scenes/End.tscn")
        _:
            pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if active:
        self.show()
        visible = true
    else:
        self.hide()
        visible = false


func show_line(line):
    if line:
        show_lines([line])

func show_lines(lines):
    if lines:
        text = lines
        _advance_text()
        active = true
