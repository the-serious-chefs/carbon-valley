extends ColorRect

signal clicked


var MouseOver = false

func _input(event):
    if event is InputEventMouseButton:
        if MouseOver == true && get_node("../../../").visible && event.is_pressed():
            emit_signal("clicked")
            MouseOver = false
            accept_event()

func _on_ColorRect_mouse_entered():
    MouseOver = true


func _on_ColorRect_mouse_exited():
    MouseOver = false
